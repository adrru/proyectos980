from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

# Create your views here.

'''
posts_listener = [
{
    'title': 'proyectos',
    'user': {
        'name': 'Clase 10/09/2019'
        'picture': 'https://cdn1.iconfinder.com/data/icons/user-pictures/100/unknown-512.png'
    }
    'cuerpo': 'Esta clase trata de mostrar una pagina posts',

}
]
'''

#def post_viewer(request):
#    return render(request,'index.html')

def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm()
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', { 'form': form})

