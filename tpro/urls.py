"""tpro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from tpro import views as local_views
from post import views as app_views
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^accounts/', include('acc.urls',namespace='accounts')),
    path('proyecto/', local_views.primera_pagina),
    path('prueba/', local_views.debugger_primario),
    #path('posts/', app_views.posts_viewer),
    url(r'^home/$', views.home, name="home"),
    path('login/', app_views.login_view),
]



urlpatterns += staticfiles_urlpatterns()