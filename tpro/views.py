from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from datetime import datetime
import json

def primera_pagina(request):
    hora = datetime.now().strftime('%b %d th, %Y - %H:%M hrs')
    return HttpResponse('Esta es nuestra primera pagina en django. y la hora es {hora}'.format(hora=hora))

def debugger_primario(request):
    numero = request.GET['numeros']
    numero= numero.split(',')
    numero = list(map(int, numero))
    numero.sort()
    #import pdb; pdb.set_trace()
    salida = {"Estado": "OK", "Numeros": numero, "Mensaje": "Numeros en JSON y ordenados de menor a Mayor"}
    return HttpResponse(json.dumps(salida, indent=4))  



def home(request):
    return render(request,'home.html')